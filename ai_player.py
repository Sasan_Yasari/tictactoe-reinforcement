import random
from tictoctoe import BOARD_FORMAT, DRAW, EMPTY, NAMES, gameover


class Agent(object):
    def __init__(self, player, verbose=False):
        self.values = {}
        self.player = player
        self.verbose = verbose
        self.epsilon = 0.1
        self.alpha = 0.5
        self.prev_state = None
        self.prev_score = 0

    def action(self, state):
        r = random.random()
        if r < self.epsilon:
            move = self.random(state)
        else:
            move = self.greedy(state)
        state[move[0]][move[1]] = self.player
        self.prev_state = tuple(state[0]), tuple(state[1]), tuple(state[2])
        self.prev_score = self.lookup(state)
        state[move[0]][move[1]] = EMPTY
        return move

    def greedy(self, state):
        max_val = -50000
        max_move = None
        if self.verbose:
            cells = []
        for i in range(3):
            for j in range(3):
                if state[i][j] == EMPTY:
                    state[i][j] = self.player
                    val = self.lookup(state)
                    state[i][j] = EMPTY
                    if val > max_val:
                        max_val = val
                        max_move = (i, j)
                    if self.verbose:
                        cells.append('{0:.3f}'.format(val).center(6))
                elif self.verbose:
                    cells.append(NAMES[state[i][j]].center(6))
        if self.verbose:
            print(BOARD_FORMAT.format(*cells))
        self.update_val(max_val)
        return max_move

    def lookup(self, state):
        key = tuple(state[0]), tuple(state[1]), tuple(state[2])
        if key not in self.values:
            self.add(key)
        return self.values[key]

    def add(self, state):
        winner = gameover(state)
        tup = tuple(state[0]), tuple(state[1]), tuple(state[2])
        self.values[tup] = self.winner_val(winner)

    def update_val(self, next_val):
        if self.prev_state is not None:
            self.values[self.prev_state] += self.alpha * (next_val - self.prev_score)

    @staticmethod
    def random(state):
        available = []
        for i in range(3):
            for j in range(3):
                if state[i][j] == EMPTY:
                    available.append((i, j))
        return random.choice(available)

    def episode_over(self, winner):
        self.update_val(self.winner_val(winner))
        self.prev_state = None
        self.prev_score = 0

    def winner_val(self, winner):
        if winner == self.player:
            return 1
        elif winner == EMPTY:
            return 0.5
        elif winner == DRAW:
            return 0
        else:
            return -1
