import csv
from ai_player import Agent
from human_player import Human
import matplotlib.pyplot as plt
from tictoctoe import PLAYER_X, PLAYER_O, play

series = ['P1-Win', 'P1-Lose', 'P1-Draw', 'P2-Win', 'P2-Lose', 'P2-Draw']
TRAIN_EPISODES = 10000


def measure_performance_vs_random(agent1, agent2):
    epsilon1 = agent1.epsilon
    epsilon2 = agent2.epsilon
    agent1.epsilon = 0
    agent2.epsilon = 0
    agent1.learning = False
    agent2.learning = False
    r1 = Agent(1)
    r2 = Agent(2)
    r1.epsilon = 1
    r2.epsilon = 1
    probs = [0, 0, 0, 0, 0, 0]
    games = 100
    for i in range(games):
        winner = play(agent1, r2)
        if winner == PLAYER_X:
            probs[0] += 1.0 / games
        elif winner == PLAYER_O:
            probs[1] += 1.0 / games
        else:
            probs[2] += 1.0 / games
    for i in range(games):
        winner = play(r1, agent2)
        if winner == PLAYER_O:
            probs[3] += 1.0 / games
        elif winner == PLAYER_X:
            probs[4] += 1.0 / games
        else:
            probs[5] += 1.0 / games
    agent1.epsilon = epsilon1
    agent2.epsilon = epsilon2
    agent1.learning = True
    agent2.learning = True
    return probs


def train(p1, p2):
    f = open('results.csv', 'w')
    writer = csv.writer(f)
    writer.writerow(series)
    perf = [[] for _ in range(len(series) + 1)]
    for i in range(TRAIN_EPISODES):
        if i % 50 == 0:
            print('Game: {0}'.format(i))
            probs = measure_performance_vs_random(p1, p2)
            writer.writerow(probs)
            f.flush()
            perf[0].append(i)
            for idx, x in enumerate(probs):
                perf[idx+1].append(x)
        winner = play(p1, p2)
        p1.episode_over(winner)
        p2.episode_over(winner)
    f.close()
    return perf


def plot(perf):
    colors = ['r','b','g','c','m','#F2C40A']
    for i in range(1,len(perf)):
        plt.plot(perf[0], perf[i], label=series[i-1], color=colors[i-1])
    plt.xlabel('Episodes')
    plt.ylabel('Probability')
    plt.title('RL Agent Performance vs. Random Agent')
    plt.legend()
    plt.savefig('selfplay_vs_random.png')


def main():
    p1 = Agent(1)
    p2 = Agent(2)
    perf = train(p1, p2)
    plot(perf)
    while True:
        p2.verbose = True
        p1 = Human(1)
        winner = play(p1, p2)
        p1.episode_over(winner)
        p2.episode_over(winner)

if __name__ == "__main__":
    main()
