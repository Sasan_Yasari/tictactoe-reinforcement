import random
from copy import copy, deepcopy
import csv
from tictoctoe import BOARD_FORMAT, DRAW, EMPTY, printboard


class Human(object):
    def __init__(self, player):
        self.player = player

    def action(self, state):
        printboard(state)
        while True:
            action = input('Your move?(y,x) ')
            move = [int(a) for a in action.split(',')]
            if move[0] < 0 or move[0] > 2 or move[1] < 0 or move[1] > 2:
                print("Error!")
            else:
                return move

    def episode_over(self, winner):
        if winner == DRAW:
            print('Game over! It was a draw.')
        else:
            print('Game over! Winner: Player {0}'.format(winner))
