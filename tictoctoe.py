BOARD_FORMAT = \
  "----------------------------\n| {0} | {1} | {2} \
|\n|--------------------------|\n| {3} | {4} | {5} \
|\n|--------------------------|\n| {6} | {7} | {8} \
|\n----------------------------"

EMPTY = 0
PLAYER_X = 1
PLAYER_O = 2
DRAW = 3
NAMES = [' ', 'X', 'O']


def play(agent1, agent2):
    state = emptystate()
    i = 0
    while True:
        if i % 2 == 0:
            move = agent1.action(state)
        else:
            move = agent2.action(state)
        if state[move[0]][move[1]] != EMPTY:
            i -= 1
        else:
            state[move[0]][move[1]] = (i % 2) + 1
            winner = gameover(state)
            if winner != EMPTY:
                return winner
        i += 1


def emptystate():
    return [[EMPTY, EMPTY, EMPTY], [EMPTY, EMPTY, EMPTY], [EMPTY, EMPTY, EMPTY]]


def gameover(state):
    for i in range(3):
        if state[i][0] != EMPTY and state[i][0] == state[i][1] and state[i][0] == state[i][2]:
            return state[i][0]
        if state[0][i] != EMPTY and state[0][i] == state[1][i] and state[0][i] == state[2][i]:
            return state[0][i]
    if state[0][0] != EMPTY and state[0][0] == state[1][1] and state[0][0] == state[2][2]:
        return state[0][0]
    if state[0][2] != EMPTY and state[0][2] == state[1][1] and state[0][2] == state[2][0]:
        return state[0][2]
    for i in range(3):
        for j in range(3):
            if state[i][j] == EMPTY:
                return EMPTY
    return DRAW


def printboard(state):
    cells = []
    for i in range(3):
        for j in range(3):
            cells.append(NAMES[state[i][j]].center(6))
    print (BOARD_FORMAT.format(*cells))
